#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include "Player.h"
#include "Character Creator.h"
#include "Map.h"
#include"Shop.h"
#include "Weapon.h"
#include "Armor.h"
#include "Monster.h"
#include <time.h>

using namespace std;



int main()
{
	Player* player = new Player();
	Monster* monster = new Monster();
	CharacterCreator* generator = new CharacterCreator();
	Map* game = new Map();
	srand(time(NULL));
	//Whole RPG Game

	cout << "Welcome to BASIC RPG!!! " << endl << endl;
	cout << "Let's start by giving yourself a Name! " << endl << endl;
	//Create Name
	player->CreateName(player);
	cout << "Now then Let's pick your class! " << endl << endl;
	generator->pickClass(player); 
	system("pause");
	system("cls");

	game->battleMap(player, monster);
	

	delete player;
	delete monster;
	delete game;

	system("pause");
	return 0;
}