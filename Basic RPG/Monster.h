#pragma once
#include <vector>
#include <string>


using namespace std;

class Player;
class Monster
{
public:
	Monster();
	~Monster();
	Monster(string monsterClass, int Hp, int exp , int damage , int vit , int dex);

	//	void mSpawn(Monster* monsterSpawnClass, int mPow, int mHp, int mExp);//Spawn monster

	//void damagePlayer(Player* mTarget);Damaging the player

	int getHp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();
	void monsterAttack(Player* player , Monster* monster);
	void takeDamage(int damage);
	int mXp();
	string getName();

private:

	string mName;
	int mHp;
	int mPow;
	int mVit;
	int mAgi;
	int mDex;
	int mChanceSpawn;
	int mHitRate;
	int mdamage;
	int mExp;

};