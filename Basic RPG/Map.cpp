#include "Map.h"
#include <iostream>
#include <string>
#include "Player.h"
#include "Monster.h"

Map::Map()
{

	
}

Map::~Map()
{



}

void Map::battleMap(Player* character , Monster* monster)
{
	//Main Menu

	int choice;

	while (character->getHp() > 0)
	{
		do
		{

			cout << "What would you like to do Traveler? " << endl;
			cout << "[1] Move" << endl;
			cout << "[2] Rest" << endl;
			cout << "[3] Check Stats" << endl;
			cout << "[4] Quit" << endl;
			cin >> choice;

			if (choice == 1)
			{
				int navChoice;
				do
				{

					cout << "Continuing the Journey! " << endl;
					cout << "Where do you want to go? " << endl;
					cout << "[1] North" << endl;
					cout << "[2] East" << endl;
					cout << "[3] South" << endl;
					cout << "[4] West" << endl;
					cout << "[5] Exit" << endl << endl;
					cin >> navChoice;
					cout << endl;

					if (navChoice == 1)
					{
						this->yAxis += 1;
						this->mSpawn(monster, character);
						system("pause");
						system("cls");
					}
					else if (navChoice == 2)
					{
						this->xAxis += 1;
						this->mSpawn(monster, character);
						system("pause");
						system("cls");
					}
					else if (navChoice == 3)
					{
						this->yAxis -= 1;
						this->mSpawn(monster, character);
						system("pause");
						system("cls");
					}
					else if (navChoice == 4)
					{
						this->xAxis -= 1;
						this->mSpawn(monster, character);
						system("pause");
						system("cls");
					}
					else if (navChoice == 5)
					{
						cout << "Exiting Navigation!!! " << endl << endl;
						system("pause");
						system("cls");
					}


				} while (navChoice != 5);

			}
			else if (choice == 2)
			{
				cout << "it's time to call it a day! " << endl << endl;
				cout << "Sleeping...Zzz...Zzz...Zzz..." << endl;
				character->doRest();
				system("pause");

			}
			else if (choice == 3)
			{
				character->displayStats();
				system("pause");
				system("cls");
			}
			else if (choice == 4)
			{
				cout << "Goodbye Traveler! Until Next time!!! " << endl;
			}


		} while (choice != 4);

		break;
	}
}


void Map::monsterBattle(Monster* monster, Player* player)
{
	while (monster->getHp() > 0 && player->getHp() > 0)
	{
		int critChoice;

		cout << player->getName() << " amount of hp left: " << player->getHp() << endl;
		cout << monster->getName() << " amount of hp left: " << monster->getHp() << endl;

		//insert here functions to fight or run away??
		do
		{
			cout << " Let's start the battle! " << endl;
			cout << "[1] Attack " << endl;
			cout << "[2] Run" << endl;
			cin >> critChoice;

		} while (critChoice >= 0 && critChoice >= 3);

		if (critChoice == 1)
		{
			//BATTLEEEEE
			//Attack
			cout << " It's your turn to attack! " << endl;
			player->playerAttack(monster, player);

			if (monster->getHp() >= 0)
			{
				cout << " It's the enemy's turn now! " << endl;
				monster->monsterAttack(player, monster);
			}
		

		}
		else if (critChoice == 2)
		{
			int runAwayChance = rand() % 100 + 1;

			if (runAwayChance <= 25)
			{
				cout << "  You've Successfully ran away!!! " << endl;
				this->battleMap(player, monster);
			}
			else if (runAwayChance > 25)
			{
				cout << " You've failed to ran away! " << endl;

			}
		}

	}
	if (monster->getHp() <= 0 && player->getHp() > 0)
	{
		cout << monster->getName() << " DIED " << endl;
		player->getXp(monster->mXp());
	}
	else if (player->getHp() <= 0)
	{
		cout << "You Died!!! " << endl;
	}

}

void Map::mSpawn(Monster* monster , Player* player)
{

	int randomMonsterSpawn = rand() % 100 + 1;

	if (randomMonsterSpawn >= 1 && randomMonsterSpawn <= 20)
	{
		cout << " No monster encountered! " << endl;
	}
	else if (randomMonsterSpawn >= 21 && randomMonsterSpawn <= 45)
	{
		cout << " You've encountered a Goblin! " << endl;
		cout << " Get ready to Fight! " << endl;
		monster = new Monster("Goblin", 10, 100, 10, 10 , 10);
		this->monsterBattle(monster, player);
	}
	else if (randomMonsterSpawn >= 46 && randomMonsterSpawn <= 70)
	{
		cout << " You've encountered a Ogre! " << endl;
		cout << " Get ready to Fight! " << endl;
		monster = new Monster("Ogre", 15, 250, 15, 15 , 15);
		this->monsterBattle(monster, player);
	}
	else if (randomMonsterSpawn >= 71 && randomMonsterSpawn <= 95)
	{
		cout << " You've encountered an Orc! " << endl;
		cout << " Get ready to Fight! " << endl;
		monster = new Monster("Orc", 20, 500, 15, 15 , 20);
		this->monsterBattle(monster, player);
	}
	else if (randomMonsterSpawn >= 96 && randomMonsterSpawn <= 100)
	{
		cout << " You've encountered an Orc Lord" << endl;
		cout << " Get ready to Fight! " << endl;
		monster = new Monster("Orc Lord", 50, 1000, 15, 25 , 20);
		this->monsterBattle(monster, player);
	}

}
