#include "Player.h"
#include "Monster.h"
#include <iostream>
#include <string>

using namespace std;

Player::Player()
{
	//Instantiates or Reconstructor
}

Player::~Player()
{
	//Deconstructor
}

void Player::CreateName(Player* player)
{
	cout << "Input Name: " << endl;
	cin >> player->name;

}

int Player::doRest()
{
	this->maxHp = 20;

	int tempHp;
	tempHp = maxHp - this->Hp;

	this->Hp += tempHp;
	
	return this->Hp;
}

string Player::getName()
{
	return this->name;
}

string Player::getClassPicked()
{
	return this->selectedClass;
}

int Player::getHp()
{
	return this->Hp;
}

int Player::getPow()
{
	return this->Pow;
}

int Player::getVit()
{
	return this->Vit;
}

int Player::getAgi()
{
	return this->Agi;
}

int Player::getDex()
{
	return this->Dex;
}

void Player::displayStats()
{
	cout << "Here are your stats! " << endl << endl;
	cout << "Player name: " << this->getName() << endl;
	cout << "Player Class: " << this->getClassPicked() << endl;
	cout << "Hp: " << this->getHp() << endl;
	cout << "Pow: " << this->getPow() << endl;
	cout << "Vit: " << this->getVit() << endl;
	cout << "Agi: " << this->getAgi() << endl;
	cout << "Dex: " << this->getDex() << endl;

}

void Player::playerAttack(Monster* monster , Player* player)
{
	float chance = ((float)Dex / (float)monster->getAgi()) * 100;
	if (chance < 20)
	{
		chance = 20;
	}
	if (chance > 80)
	{
		chance = 80;
	}

	cout << player->getName()<< " Attacked " << endl;

	int randomHit = rand() % 100 + 1;
	if (randomHit <= chance)
	{
		cout << player->getName() << " Landed a Hit " << endl;
		int damage = player->getPow() - monster->getVit();
		if (damage < 1)
		{
			damage = 1;
		}
		cout << player->getName() << " Dealt " << damage << endl;
		monster->takeDamage(damage);

	}
	else 
	{
		cout << player->getName() << " Missed " << endl;
	}

}

void Player::takeDamage(int damage)
{
	this->Hp -= damage;
	if (Hp < 0)
	{
		Hp = 0;
	}
}


void Player::levelUp()
{
	int randomStatBoost = rand() % 100 + 1;

	this->addHp(randomStatBoost);
	this->addPow(randomStatBoost);
	this->addDex(randomStatBoost);
	this->addVit(randomStatBoost);
	this->addAgi(randomStatBoost);
}

void Player::getXp(int xp)
{
	int requiredLevel = this->level * 1000;

	this->xP += xp;
	if (this->xP >= requiredLevel)
	{
		levelUp();
	}
}


string Player::addClassName(string adder)
{
	this->selectedClass = adder;
	return this->selectedClass;
}

int Player::addHp(int adder)
{
	this->Hp = adder;
	return this->Hp;
}

int Player::addPow(int adder)
{
	this->Pow = adder;
	return this->Pow;
}

int Player::addVit(int adder)
{
	this->Vit = adder;
	return this->Vit;
}

int Player::addAgi(int adder)
{
	this->Agi = adder;
	return this->Agi;
}

int Player::addDex(int adder)
{
	this->Dex = adder;
	return this->Dex;
}





