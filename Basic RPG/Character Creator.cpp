#include "Character Creator.h"
#include "Player.h"
#include <iostream>

using namespace std;

CharacterCreator::CharacterCreator()
{


}

CharacterCreator::~CharacterCreator()
{


}


void CharacterCreator::pickClass(Player* player)
{
	int choice;

	cout << "Select Class " << endl;
	cout << "[1] Fighter " << endl;
	cout << "[2] Wizard " << endl;
	cout << "[3] Cleric " << endl;
	cout << "[4] Thief " << endl;
	cin >> choice;

	if (choice == 1)
	{
		cout << "You've chosen the Fighter Class!!! " << endl;

		this->charClass = "Fighter";
		this->charHp = 15;
		this->charPow = 8;
		this->charDex = 7;
		this->charVit = 7;
		this->charAgi = 9;

		player->addClassName(this->charClass);
		player->addHp(this->charHp);
		player->addPow(this->charPow);
		player->addDex(this->charDex);
		player->addVit(this->charVit);
		player->addAgi(this->charAgi);


	}
	else if (choice == 2)
	{
		cout << "You've chosen the Wizard Class!!! " << endl;
		
		this->charClass = "Wizard";
		this->charHp = 15;
		this->charPow = 15;
		this->charDex =15;
		this->charVit = 15;
		this->charAgi = 15;

		player->addClassName(this->charClass);
		player->addHp(this->charHp);
		player->addPow(this->charPow);
		player->addDex(this->charDex);
		player->addVit(this->charVit);
		player->addAgi(this->charAgi);

	}
	else if (choice == 3)
	{
		cout << "You've chosen the Cleric Class!!! " << endl;

		this->charClass = "Cleric";
		this->charHp = 15;
		this->charPow = 15;
		this->charDex = 9;
		this->charVit = 10;
		this->charAgi = 5;

		player->addClassName(this->charClass);
		player->addHp(this->charHp);
		player->addPow(this->charPow);
		player->addDex(this->charDex);
		player->addVit(this->charVit);
		player->addAgi(this->charAgi);

	}
	else if (choice == 4)
	{
		cout << "You've chosen the Thief Class!!! " << endl;

		this->charClass = "Thief";
		this->charHp = 15;
		this->charPow = 6;
		this->charDex = 7;
		this->charVit = 8;
		this->charAgi = 15;

		player->addClassName(this->charClass);
		player->addHp(this->charHp);
		player->addPow(this->charPow);
		player->addDex(this->charDex);
		player->addVit(this->charVit);
		player->addAgi(this->charAgi);

	}
	
}
