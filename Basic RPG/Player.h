#pragma once
#include <vector>
#include <string>

using namespace std;

class Monster;
class Player 
{
public:
	
	Player();
	~Player();

	void CreateName(Player* player); // this creates the players name
	int doRest();

	//Accessors

	string getName();
	string getClassPicked();
	int getHp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();
	void displayStats();
	void playerAttack(Monster* monster , Player* player);
	void takeDamage(int damage);
	void levelUp();
	void getXp(int xp);

	//Adders
	string addClassName(string adder);
	int addHp(int adder);
	int addPow(int adder);
	int addVit(int adder);
	int addAgi(int adder);
	int addDex(int adder);

private:

	string name;
	string selectedClass;
	int Hp;
	int Pow;
	int Vit;
	int Agi;
	int Dex;
	int maxHp;
	int xP = 0;
	int level = 1;

};
