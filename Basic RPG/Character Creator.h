#pragma once
#include <vector>
#include <string>

using namespace std;

class Player;
class CharacterCreator 
{
public:

	CharacterCreator();
	~CharacterCreator();


	void pickClass(Player* player); // Choosing a class

private:

	string charClass;
	int charHp;
	int charPow;
	int charVit;
	int charAgi;
	int charDex;

};

