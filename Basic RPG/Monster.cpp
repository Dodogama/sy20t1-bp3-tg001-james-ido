#include "Monster.h"
#include "Player.h"
#include "Map.h"
#include <iostream>
#include <string>

Monster::Monster()
{

}


Monster::~Monster()
{

}

Monster::Monster(string monsterClass, int Hp, int exp, int damage , int vit , int dex )
{
	this->mName = monsterClass;
	this->mHp = Hp;
	this->mExp = exp;
	this->mdamage = damage;
	this->mVit = vit;
	this->mDex = dex;
	
}

int Monster::getHp()
{
	return this->mHp;
}

int Monster::getPow()
{
	return this->mPow;
}

int Monster::getVit()
{
	return this->mVit;
}

int Monster::getAgi()
{
	return this->mAgi;
}

int Monster::getDex()
{
	return this->mDex;
}

void Monster::monsterAttack(Player* player , Monster* monster)
{
	float chance = ((float)mDex / (float)player->getAgi()) * 100;
	if (chance < 20)
	{
		chance = 20;
	}
	if (chance > 80)
	{
		chance = 80;
	}

	cout << monster->mName << " Attacked " << endl;

	int randomHit = rand() % 100 + 1;
	if (randomHit <= chance)
	{
		cout << monster->getName() << " Landed a Hit " << endl;
		int damage = monster->getPow() - player->getVit();
		if (damage < 1)
		{
			damage = 1;
		}
		cout << monster->getName() << " Dealt " << damage << endl;
		player->takeDamage(damage);
	}
	else
	{
		cout << monster->getName() << " Missed " << endl;
	}

}

void Monster::takeDamage(int damage)
{
	this->mHp -= damage;
	if (mHp < 0)
	{
		mHp = 0;
	}
}

int Monster::mXp()
{
	return this->mExp;
}

string Monster::getName()
{
	return this->mName;
}



