#pragma once
#include <vector>
#include <string>

using namespace std;

class Player;
class Monster;
class Map 
{
public:

	Map();
	~Map();

	void battleMap(Player* character , Monster* monster); // Map movement and battle
	void monsterBattle(Monster* monster ,Player* player) ; // Monster spawner
	void mSpawn(Monster* monster, Player* player);
private:

	int xAxis;
	int yAxis;

};